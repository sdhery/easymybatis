package net.oschina.durcframework.easymybatis;

public interface HasPk<T> {
	T getPk();
}
