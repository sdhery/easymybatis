/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.oschina.durcframework.easymybatis.ext.code.client;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.velocity.VelocityContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import net.oschina.durcframework.easymybatis.ext.code.generator.SQLContext;
import net.oschina.durcframework.easymybatis.ext.code.generator.TableDefinition;
import net.oschina.durcframework.easymybatis.ext.code.generator.TableSelector;
import net.oschina.durcframework.easymybatis.ext.code.util.VelocityUtil;

/**
 * 代码生成器，根据定义好的velocity模板生成代码
 * @author tanghc
 *
 */
public class Generator {
	
	public String generateCode(ClientParam clientParam) throws FileNotFoundException {
		InputStream templateInputStream = this.buildTemplateInputStream(clientParam.getTemplateClasspath());

		SQLContext sqlContext = this.buildClientSQLContextList(clientParam);
		
		String content = doGenerator(sqlContext, templateInputStream);

		return content;
	}
	
	// 返回模板文件内容
	private InputStream buildTemplateInputStream(String templateClasspath) throws FileNotFoundException {
		Resource resource = new ClassPathResource(templateClasspath);
		try {
			return resource.getInputStream();
		} catch (IOException e) {
			throw new FileNotFoundException(e.getMessage());
		}
	}
	
	/**
	 * 返回SQL上下文列表
	 * 
	 * @param tableNames
	 * @return
	 */
	private SQLContext buildClientSQLContextList(ClientParam clientParam) {
		Class<?> entityClass = clientParam.getEntityClass();
		
		TableSelector tableSelector = new TableSelector(entityClass);

		TableDefinition tableDefinition = tableSelector.getTableDefinition();

		SQLContext context = new SQLContext(tableDefinition);
		
		String namespace = this.buildNamespace(clientParam.getMapperClass());
		context.setClassName(entityClass.getName());
		context.setClassSimpleName(entityClass.getSimpleName());
		context.setPackageName(entityClass.getPackage().getName());
		context.setNamespace(namespace);
		
		return context;
	}
	
	private String buildNamespace(Class<?> mapperClass) {
		return mapperClass.getName();
	}

	private String doGenerator(SQLContext sqlContext, InputStream inputStream) {
		VelocityContext context = new VelocityContext();

		TableDefinition tableDefinition = sqlContext.getTableDefinition();
		
		context.put("context", sqlContext);
		context.put("table", tableDefinition);
		context.put("pk", tableDefinition.getPkColumn());
		context.put("columns", tableDefinition.getTableColumns());
		context.put("allColumns", tableDefinition.getAllColumns());

		return VelocityUtil.generate(context, inputStream);
	}

}
