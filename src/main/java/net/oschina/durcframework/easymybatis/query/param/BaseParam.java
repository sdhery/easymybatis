/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.oschina.durcframework.easymybatis.query.param;

import net.oschina.durcframework.easymybatis.query.Query;

public class BaseParam implements SchPageSort {

	// 当前第几页
	private int page = 1;
	// 每页记录数
	private int rows = 20;

	private String sort;
	private String order;
	
	public Query toQuery() {
		return Query.build(this);
	}

	@Override
	public String getSortname() {
		return sort;
	}

	@Override
	public String getSortorder() {
		return order;
	}

	@Override
	public int getStart() {
		return (int) ((page - 1) * rows);
	}

	@Override
	public int getLimit() {
		return rows;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	@Override
	public String getDBSortname() {
		return this.sort;
	}

}
