/*
 * Copyright 2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package net.oschina.durcframework.easymybatis.query.expression.builder;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.oschina.durcframework.easymybatis.query.annotation.LikeDoubleField;
import net.oschina.durcframework.easymybatis.query.annotation.LikeLeftField;
import net.oschina.durcframework.easymybatis.query.annotation.LikeRightField;
import net.oschina.durcframework.easymybatis.query.annotation.ListField;
import net.oschina.durcframework.easymybatis.query.annotation.ValueField;
import net.oschina.durcframework.easymybatis.query.expression.Expression;


/**
 * 从bean中获取Expression
 */
public class QueryBuilder {

	private static final String PREFIX_GET = "get";

	// init
	static {
		AnnoExprStore.addExpressionGetter(ListField.class,new ListExpressionGetter());
		AnnoExprStore.addExpressionGetter(ValueField.class,new ValueExpressionGetter());
		AnnoExprStore.addExpressionGetter(LikeLeftField.class,new LikeLeftExpressionGetter());
		AnnoExprStore.addExpressionGetter(LikeRightField.class,new LikeRightExpressionGetter());
		AnnoExprStore.addExpressionGetter(LikeDoubleField.class,new LikeDoubleExpressionGetter());
	}

	/**
	 * 获取条件表达式
	 * @param obj
	 * @return
	 */
	public static List<Expression> buildExpressions(Object obj) {
		if (obj == null) {
			return null;
		}
		List<Expression> expList = new ArrayList<Expression>();
		Method[] methods = obj.getClass().getDeclaredMethods();
		try {
			for (Method method : methods) {
				String methodName = method.getName();
				Annotation[] annotations = method.getAnnotations();

				if (couldBuildExpression(methodName, annotations)) {
					Object value = method.invoke(obj, new Object[] {});

					expList.addAll(buildExpression(annotations, value));
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return expList;
	}

	private static List<Expression> buildExpression(Annotation[] annotations, Object value) {
		List<Expression> expList = new ArrayList<Expression>();

		for (Annotation annotation : annotations) {

			ExpressionGetter expressionGetter = AnnoExprStore.get(annotation);
			
			Expression expression = expressionGetter.buildExpression(
					annotation, value);

			if (expression != null) {
				expList.add(expression);
			}
		}

		return expList;
	}

	// 能否构建表达式
	private static boolean couldBuildExpression(String methodName,
			Annotation[] annotations) {
		return methodName.startsWith(PREFIX_GET)
				&& hasExpressionAnnotation(annotations);
	}

	// 是否有注解
	private static boolean hasExpressionAnnotation(Annotation[] annotations) {
		if (annotations == null || annotations.length == 0) {
			return false;
		}
		for (Annotation annotation : annotations) {
			if (AnnoExprStore.get(annotation) != null) {
				return true;
			}
		}
		return false;
	}
	
	static class AnnoExprStore {
		
		private static Map<String, ExpressionGetter> map = new HashMap<String, ExpressionGetter>();
		
		/**
		 * 通过注解获取
		 * @param annotation
		 * @return
		 */
		public static ExpressionGetter get(Annotation annotation){
			return map.get(annotation.annotationType().getSimpleName());
		}
		
		/**
		 * 保存
		 * @param clazz 注解的class
		 * @param getter ExpressionGetter
		 */
		public static void addExpressionGetter(Class<?> clazz,ExpressionGetter getter) {
			map.put(clazz.getSimpleName(), getter);
		}
	}

}
